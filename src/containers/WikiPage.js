import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import ScrollArea from 'react-scrollbar'

import { closeWikiPage } from '../actions/index'

class WikiPage extends Component {
	constructor(props) {
		super(props)

		this.closePopup = this.closePopup.bind(this)
	}

	closePopup() {
		this.props.closeWikiPage()
	}

	renderWikiPage(body) {
		body = body.split('== References ==')[0]
		body = body.split('== See also ==')[0]
		body = body.split("\n")

		return body.map((line, i) => {
			if (line === '') return null
			else if (/^={3} .* ={3}$/.test(line)) return <h3 key={i}>{line.replace(/={3}/g, '')}</h3>
			else if (/^={2} .* ={2}$/.test(line)) return <h2 key={i}>{line.replace(/={2}/g, '')}</h2>
			else return <p key={i}>{line}</p>
		})
	}

	render() {
		const { wikiPage } = this.props.general

		return (
			<div>
				<div className={wikiPage !== '' ? 'bg show' : 'bg'} onClick={() => this.closePopup()} />
				<div className={wikiPage !== '' ? 'popup show' : 'popup'}>
					<div className="popup-header">
						<h3 className="popup-title">
							{wikiPage !== '' ? wikiPage.title : ''}
						</h3>
						<i className="material-icons" onClick={() => this.closePopup()}>close</i>
					</div>
					<ScrollArea className="popup-content-scroll-wrap" contentClassName="popup-content">
						<div>
							{wikiPage !== '' ? this.renderWikiPage(wikiPage.extract) : ''}
						</div>
					</ScrollArea>
				</div>
			</div>
		)
	}
}

const mapStateToProps = ({ general }) => ({
	general
})

const mapDispatchToProps = dispatch => bindActionCreators({closeWikiPage}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(WikiPage)


import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { searchTerm } from '../actions/index'

class AppBar extends Component {
	constructor(props) {
		super(props)

		this.onInputChange = this.onInputChange.bind(this)
	}

	onInputChange(e) {
		e.preventDefault()

		this.props.searchTerm(e.target.value)
	}

	render() {
		return (
			<div className="app-bar">
				<form className="search-bar-wrap" onSubmit={e => e.preventDefault()}>
					<input
						className="search-bar-input"
						onChange={this.onInputChange}
						value={this.props.general.q}
						placeholder="Szukaj..."/>
				</form>
			</div>
		);
	}
}

const mapStateToProps = ({ general }) => ({
	general
})

const mapDispatchToProps = dispatch => bindActionCreators({searchTerm}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(AppBar)
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import axios from 'axios'

import { selectTerm } from '../actions/index'

import Card from "../components/Card";

class CardsGrid extends Component {
	constructor(props) {
		super(props)

		this.onTermSelect = this.onTermSelect.bind(this)
	}

	onTermSelect(url) {
		// Wikipedia page title
		const titles = url.split('/')[4]

		axios.get('/w/api.php', {
			baseURL: 'https://en.wikipedia.org',
			params: {
				action: 'query',
				format: 'json',
				prop: 'extracts',
				formatversion: 2,
				origin: '*',
				explaintext: 1,
				titles
			}
		})
			.then(res => this.props.selectTerm(res))
			.catch(err => {
				if (err.response)
					console.log(err.response.data)
			})
	}

	render() {
		return (
			<div className="card-grid">
				{
					this.props.terms.map((term, i) => {
						const { title, category, url } = term
						if (term.display)
							return (<Card key={i}
							              onClick={() => this.onTermSelect(url)}
							              title={title}
							              category={category}/>)
					})
				}
			</div>
		)
	}
}

const mapStateToProps = ({terms}) => ({
	terms
})

const mapDispatchToProps = dispatch => bindActionCreators({selectTerm}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(CardsGrid)
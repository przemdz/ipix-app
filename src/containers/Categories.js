import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import ScrollArea from 'react-scrollbar'

import { selectCategory } from '../actions/index'

class Categories extends Component {
	constructor(props) {
		super(props)

		this.onCategorySelect = this.onCategorySelect.bind(this)
	}

	onCategorySelect(index, category) {
		this.props.selectCategory(index, category)
	}

	render() {
		return (
			<ScrollArea className="categories-list">
				<ul>
					<li onClick={() => this.onCategorySelect(-1)}
					    className={this.props.general.selectedCategory === -1 ? 'selected' : ''}>
						Wszystkie
					</li>
					{
						this.props.categories.map((category, i) => {
							if (category.name !== '')
								return (
									<li key={i}
										onClick={() => this.onCategorySelect(i, category.name)}
										className={this.props.general.selectedCategory === i ? 'selected' : ''}>
										{category.name}
									</li>
								)
							}
						)
					}
				</ul>
			</ScrollArea>
		)
	}
}

const mapStateToProps = ({ categories, general }) => ({
	categories,
	general
})

const mapDispatchToProps = dispatch => bindActionCreators({selectCategory}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Categories)


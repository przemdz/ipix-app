import { combineReducers } from 'redux'

// reducers
import terms from './terms'
import categories from './categories'
import general from './general'

const ipixApp = combineReducers({
	terms,
	categories,
	general
})

export default ipixApp
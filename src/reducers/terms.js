import _ from 'lodash'

import terms from '../data/specjalizacje.csv'

import { SEARCH_TERM, SELECT_CATEGORY } from '../constants'

const initialState = _.sortBy(_.map(terms, term => ({
	title: term[0],
	url: term[1],
	category: term[2],
	display: true
})), ['title'])

export default (state = initialState, action) => {
	switch (action.type) {
		case SEARCH_TERM:
			return _.map(state, term => ({
				...term,
				display: _.includes(_.toLower(term.title), _.toLower(action.q)) ||
				_.includes(_.toLower(term.category), _.toLower(action.q))
			}))
		case SELECT_CATEGORY:
			if (action.index > -1) {
				return _.map(state, term => ({
					...term,
					display: _.includes(_.toLower(term.category), _.toLower(action.category))
				}))
			} else {
				return _.map(state, term => ({
					...term,
					display: true
				}))
			}
			return state
		default:
			return state
	}

}
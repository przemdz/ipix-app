import { SEARCH_TERM, SELECT_CATEGORY, SELECT_TERM, CLOSE_WIKI_PAGE } from '../constants'

const initialState = {
	// index of selected category
	selectedCategory: -1,
	// search query
	q: '',
	// fetch wikipedia page
	wikiPage: ''
}

export default (state = initialState, action) => {
	switch (action.type) {
		case SEARCH_TERM:
			return {
				...state,
				q: action.q,
				selectedCategory: -1
			}
		case SELECT_CATEGORY:
			return {
				...state,
				q: '',
				selectedCategory: action.index
			}
		case SELECT_TERM:
			const { title, extract } = action.res.data.query.pages[0]

			return {
				...state,
				wikiPage: {
					title,
					extract
				}
			}
		case CLOSE_WIKI_PAGE:
			return {
				...state,
				wikiPage: ''
			}
		default:
			return state
	}
}
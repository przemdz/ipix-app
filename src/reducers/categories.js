import _ from 'lodash'

import terms from '../data/specjalizacje.csv'

// sorted categories
const initialState = _.sortBy(_.uniqBy(_.map(terms, term => ({
	name: term[2]
})), 'name'), 'name')


export default (state = initialState, action) => state
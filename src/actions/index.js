import { SEARCH_TERM, SELECT_CATEGORY, SELECT_TERM, CLOSE_WIKI_PAGE } from '../constants'

export const searchTerm = q => ({
	type: SEARCH_TERM,
	q
})

export const selectCategory = (index, category = '') => ({
	type: SELECT_CATEGORY,
	index,
	category
})

export const selectTerm = res => ({
	type: SELECT_TERM,
	res
})

export const closeWikiPage = () => ({
	type: CLOSE_WIKI_PAGE
})
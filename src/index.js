import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { createStore } from 'redux'

// reducers root
import reducers from './reducers'

// components
import App from './components/App'

// style
import style from './style/main.scss'

render(
    <Provider store={createStore(reducers)}>
        <App />
    </Provider>,
    document.getElementById('app')
)
import React from 'react'

export default props => {
	const { title, category, onClick } = props

	return (
		<div className="card-wrap" onClick={onClick}>
			<h3>{title}</h3>
			<p>{category}</p>
		</div>
	)
}
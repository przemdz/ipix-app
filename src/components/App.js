import React from 'react'

// components
import AppBar from '../containers/AppBar'
import CardsGrid from '../containers/CardsGrid'
import Categories from '../containers/Categories'
import WikiPage from '../containers/WikiPage'

export default () => ((
	<div>
		<AppBar />
		<div className="main">
			<Categories />
			<CardsGrid />
		</div>
		<WikiPage />
	</div>
))
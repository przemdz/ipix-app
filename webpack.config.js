var webpack = require('webpack');
var path = require('path');

module.exports = {
	entry: './src/index.js',
	output: {
		path: __dirname,
		filename: 'bundle.js'
	},
	devtool: 'eval',
	module: {
		rules: [{
			test: /\.js$/,
			exclude: /node_modules/,
			loader: 'babel-loader'
		},
			{
				test: /\.scss$/,
				use: [{
					loader: 'style-loader'
				},
				{
					loader: 'css-loader'
				},
				{
					loader: 'sass-loader'
				}]
			},
			{
				test: /\.csv$/,
				loader: 'dsv-loader',
				query: {
					delimiter: '\t',
					rows: true
				}
			}]
	},
	resolve: {
		extensions: ['.js', '.scss', '.csv']
	},
	devServer: {
		contentBase: path.join(__dirname, "public"),
		port: 8000
	}
};
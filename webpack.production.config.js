var webpack = require('webpack');
var path = require('path');

module.exports = {
	entry: './src/index.js',
	output: {
		publicPath: './',
		path: path.join(__dirname, 'public'),
		filename: 'bundle.js'
	},
	devtool: 'source-map',
	module: {
		rules: [{
			test: /\.js$/,
			exclude: /node_modules/,
			loader: 'babel-loader'
		},
			{
				test: /\.scss$/,
				use: [{
					loader: 'style-loader'
				},
					{
						loader: 'css-loader'
					},
					{
						loader: 'sass-loader'
					}]
			},
			{
				test: /\.csv$/,
				loader: 'dsv-loader',
				query: {
					delimiter: '\t',
					rows: true
				}
			}]
	},
	plugins: [
		new webpack.DefinePlugin({
			'process.env': {
				NODE_ENV: '"production"'
			}
		}),
		new webpack.optimize.OccurrenceOrderPlugin(),
		new webpack.optimize.UglifyJsPlugin({
			compress: {
				warnings: false,
				screw_ie8: true,
				drop_console: true,
				drop_debugger: true
			}
		})
	],
	resolve: {
		extensions: ['.js', '.scss', '.csv']
	}
};